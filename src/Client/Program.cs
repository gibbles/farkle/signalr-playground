﻿using Microsoft.AspNetCore.SignalR.Client;

// See https://aka.ms/new-console-template for more information

var username = "shane";

var connection = new HubConnectionBuilder()
    .WithUrl("http://localhost:5247/ChatHub")
    .WithAutomaticReconnect()
    .Build();

connection.Closed += async (error) =>
{
    await Task.Delay(new Random().Next(0, 5) * 1000);
    await connection.StartAsync();
};

connection.On<string, string>("ReceiveMessage", (user, message) =>
{
    Console.WriteLine($"{user}: {message}");
});

await connection.StartAsync();
Console.WriteLine("Connection started");

string message = "primer";
while(!string.IsNullOrEmpty(message))
{
    message = Console.ReadLine();
    await connection.InvokeAsync("SendMessage", username, message);
}
